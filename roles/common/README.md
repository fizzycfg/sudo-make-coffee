# Common

Common configuration.

## Requirements

None.

## Role Variables

None.

## Dependencies

None.

## Example Playbook

This is an example to include the role `common` in your `site.yml` file:

    - hosts: myhosts
      roles:
         - role: common

## License

Apache version 2.0.

## Author Information

The Fizzy team!
