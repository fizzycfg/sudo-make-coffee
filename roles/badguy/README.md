# Bad Guy

Configuration for the bad guys :)

## Requirements

None.

## Role Variables

None.

## Dependencies

`badguy` depends on the following roles:

-   `common`

## Example Playbook

This is an example to include the role `badguy` in your `site.yml` file:

    - hosts: myhosts
      roles:
         - role: badguy

## License

Apache version 2.0.

## Author Information

The Fizzy team!
