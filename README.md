# sudo make coffee

The real coffee maker!

## Documentation

See [wiki](https://gitlab.com/fizzycfg/sudo-make-coffee/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
