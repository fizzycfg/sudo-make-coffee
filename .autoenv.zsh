local root="${autoenv_env_file:h}"

if [[ ! -d "${root}/.venv" ]]; then
  virtualenv -p "$(which python3)" "${root}/.venv"
fi

source "$(pwd)/.venv/bin/activate"

pip install -r requirements.txt 2>&1 > /dev/null
